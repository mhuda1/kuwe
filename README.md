# Kuwe.id
**Kuwe.id** adalah website simulator untuk pembuatan kue. 
Disini user akan menghias sendiri kreasi kuenya lengkap dengan resep dari kue tersebut.
Ini adalah projek akhir untuk mata kuliah Advanced Programming dari mahasiswa Fakultas Ilmu Komputer Universitas Indonesia.

## Kelompok 5 Advanced Programming 
<a href="https://gitlab.com/mhuda1/kuwe/-/commits/master"><img alt="pipeline status" src="https://gitlab.com/mhuda1/kuwe/badges/master/pipeline.svg" /></a>  <a href="https://gitlab.com/mhuda1/kuwe/-/commits/master"><img alt="coverage report" src="https://gitlab.com/mhuda1/kuwe/badges/master/coverage.svg" /></a>

### Anggota Kelompok:
- Mushaffa Huda
- Muhammad Iqbal Wijonarko
- Muhammad Zakiy Saputra
- Nurul Srianda Putri
- Fandy David Rivaldy