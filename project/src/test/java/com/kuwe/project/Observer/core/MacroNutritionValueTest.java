package com.kuwe.project.Observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class MacroNutritionValueTest {

    private MacroNutrition macroNutrition;

    @Mock
    private Game kue;

    @BeforeEach
    public void setUp() {
        macroNutrition = new MacroNutrition(kue);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptmenu1Type() {
        Menu menu1 = new Menu();
        menu1.setNamaBahan("Coklat");
        menu1.setTipeNutrisi("MacroNutrition");
        when(kue.getMenu()).thenReturn(menu1);

        macroNutrition.updateMenu();
        List<Menu> macroNutritionMenuList = macroNutrition.getMenus();

        assertThat(macroNutritionMenuList).contains(menu1);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptmenu2Type() {
        Menu menu2 = new Menu();
        menu2.setNamaBahan("Buah");
        menu2.setTipeNutrisi("MacroNutrition");
        when(kue.getMenu()).thenReturn(menu2);

        macroNutrition.updateMenu();
        List<Menu> macroNutritionMenuList = macroNutrition.getMenus();

        assertThat(macroNutritionMenuList).contains(menu2);
    }

    @Test
    public void testTypeNutrition() {
        Menu menu2 = new Menu();
        menu2.setNamaBahan("Vanilla");
        menu2.setTipeNutrisi("MacroNutrition");

        macroNutrition.updateMenu();

        assertEquals(menu2.getTipeNutrisi(), "MacroNutrition");
    }

    @Test
    public void testNamaBahan() {
        Menu menu2 = new Menu();
        menu2.setNamaBahan("Dark Chocolate");
        menu2.setTipeNutrisi("MacroNutrition");

        macroNutrition.updateMenu();

        assertEquals(menu2.getNamaBahan(), "Dark Chocolate");
    }



}
