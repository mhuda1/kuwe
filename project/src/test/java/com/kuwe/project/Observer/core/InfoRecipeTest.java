package com.kuwe.project.Observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class InfoRecipeTest {

    private InfoRecipe infoRecipe;
    private Game kue;

    @BeforeEach
    public void setUp() {
        kue = new Game();
        infoRecipe = new InfoRecipe(kue);
    }

    @Test
    public void usesSpoonacularAPI() {
        assertEquals("spoonacular", infoRecipe.getApi());
    }

    @Test
    public void recipeShouldOnlyUpdateWhenDone() {
        kue.addIngredient("Stroberi");
        kue.addIngredient("Kelapa");
        assertEquals(0, infoRecipe.getIngredients().size());
        assertNotEquals(kue.getIngredients().size(), infoRecipe.getIngredients().size());
        kue.setGameStatus();
        kue.addIngredient("Kue");
        assertEquals(kue.getIngredients().size(), infoRecipe.getIngredients().size());
    }
}