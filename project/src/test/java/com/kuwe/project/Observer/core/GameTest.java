package com.kuwe.project.Observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

public class GameTest {
    private Game game;

    @BeforeEach
    public void setUp() {
        game = new Game();
    }

    @Test
    public void testWhenAddMenuIsCalledItShouldSetTheMenuProperty(){
        Menu menu = new Menu();
        menu.setNamaBahan("Coklat");

        game.addMenu(menu);

        assertThat(game.getMenu()).isSameAs(menu);
    }

    @Test
    public void testTipeNutrisiMenu(){
        Menu menu = new Menu();
        menu.setNamaBahan("Vanilla Banana");
        menu.setTipeNutrisi("MicroNutrition");

        game.addMenu(menu);
        String tipe = menu.getTipeNutrisi();

        assertEquals(tipe, "MicroNutrition");
    }


    @Test
    public void testGameNotDone() {
        assertEquals("not Done", game.getGameStatus());
    }

    @Test
    public void testGameDone() {
        game.setGameStatus();
        assertEquals("Done", game.getGameStatus());
    }

    @Test
    public void testIngredientListDifferentWhenAdded() {
        game.addIngredient("Wortel");
        int previousIngredients = game.getIngredients().size();
        game.addIngredient("Krim");
        assertNotEquals(previousIngredients, game.getIngredients().size());
    }
}
