package com.kuwe.project.Observer.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class MicroNutritionValueTest {
    
    private MicroNutrition microNutrition;

    @Mock
    private Game kue;

    @BeforeEach
    public void setUp() {
        microNutrition = new MicroNutrition(kue);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptmenu1Type() {
        Menu menu1 = new Menu();
        menu1.setNamaBahan("Bolu");
        menu1.setTipeNutrisi("MicroNutrition");
        when(kue.getMenu()).thenReturn(menu1);

        microNutrition.updateMenu();
        List<Menu> MicroNutritionMenuList = microNutrition.getMenus();

        assertThat(MicroNutritionMenuList).contains(menu1);
    }

    @Test
    public void whenUpdateMethodIsCalledItShouldAcceptmenu2Type() {
        Menu menu2 = new Menu();
        menu2.setNamaBahan("Regal");
        menu2.setTipeNutrisi("MicroNutrition");
        when(kue.getMenu()).thenReturn(menu2);

        microNutrition.updateMenu();
        List<Menu> MicroNutritionMenuList = microNutrition.getMenus();

        assertThat(MicroNutritionMenuList).contains(menu2);
    }

    @Test
    public void testTypeNutrition() {
        Menu menu2 = new Menu();
        menu2.setNamaBahan("Cream");
        menu2.setTipeNutrisi("MacroNutrition");

        microNutrition.updateMenu();

        assertEquals(menu2.getTipeNutrisi(), "MacroNutrition");
    }

    @Test
    public void testNamaBahan() {
        Menu menu2 = new Menu();
        menu2.setNamaBahan("Avocado");
        menu2.setTipeNutrisi("MacroNutrition");

        microNutrition.updateMenu();

        assertEquals(menu2.getNamaBahan(), "Avocado");
    }

}
