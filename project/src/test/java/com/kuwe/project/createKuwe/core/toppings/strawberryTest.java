package com.kuwe.project.createKuwe.core.toppings;

import com.kuwe.project.createKuwe.core.kuwe.Coklat;
import com.kuwe.project.createKuwe.core.kuwe.Vanilla;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class strawberryTest {
    private strawberry strawberry;

    @BeforeEach
    public void setUp(){
        strawberry = new strawberry(new Vanilla());
    }

    @Test
    public void testMethodGetKuweName(){
        String name = strawberry.getNamaKuwe();
        assertEquals("Kuwe Vanilla", name);
    }

    @Test
    public void testGetMethodKuweDescription(){
        String desc = strawberry.getKuweDescription();
        assertEquals("Kuwe vanilla maknyus", desc);
    }

    @Test
    public void testMethodGetKuweToppings(){
        ArrayList<String> toppings= strawberry.getToppings();
        assertTrue(toppings.contains("Strawberry"));
    }
}
