package com.kuwe.project.createKuwe.core.toppings;

import com.kuwe.project.createKuwe.core.kuwe.Coklat;
import com.kuwe.project.createKuwe.core.kuwe.Kuwe;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class chocolateTest {

    private chocolate chocolate;

    @BeforeEach
    public void setUp(){
        chocolate = new chocolate(new Coklat());
    }

    @Test
    public void testMethodGetKuweName(){
        String name = chocolate.getNamaKuwe();
        assertEquals("Kuwe Coklat", name);
    }

    @Test
    public void testGetMethodKuweDescription(){
        String desc = chocolate.getKuweDescription();
        assertEquals("Kuwe coklat enak bat mantap!!", desc);
    }

    @Test
    public void testMethodGetKuweToppings(){
        ArrayList<String> toppings= chocolate.getToppings();
        assertTrue(toppings.contains("Chocolate"));
    }

}
