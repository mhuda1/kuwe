package com.kuwe.project.createKuwe.core.kuwe;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CoklatTest {

    private Kuwe kuwe;

    @BeforeEach
    public void setUp(){
        kuwe = new Coklat();
    }

    @Test
    public void testMethodGetKuweName(){
        assertEquals("Kuwe Coklat", kuwe.getNamaKuwe());
    }

    @Test
    public void testMethodGetKuweDescription(){
        assertEquals("Kuwe coklat enak bat mantap!!", kuwe.getKuweDescription());
    }

    @Test
    public void testMethodGetKuweTopping(){
         assertTrue(kuwe.getToppings().isEmpty());
    }

}
