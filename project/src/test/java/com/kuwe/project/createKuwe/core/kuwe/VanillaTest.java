package com.kuwe.project.createKuwe.core.kuwe;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VanillaTest {

    private Kuwe kuwe;

    @BeforeEach
    public void setUp(){
        kuwe = new Vanilla();
    }

    @Test
    public void testMethodGetKuweName(){
        assertEquals("Kuwe Vanilla", kuwe.getNamaKuwe());
    }

    @Test
    public void testMethodGetKuweDescription(){
        assertEquals("Kuwe vanilla maknyus", kuwe.getKuweDescription());
    }

    @Test
    public void testMethodGetKuweTopping(){
        assertTrue(kuwe.getToppings().isEmpty());
    }
}
