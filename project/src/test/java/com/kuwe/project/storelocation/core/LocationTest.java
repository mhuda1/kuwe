package com.kuwe.project.storelocation.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LocationTest {
    private Location location;

    @BeforeEach
    public void setUp() {
        location = new Location();
    }

    @Test
    public void testSetLatitude() {
        double latitude = 123;
        location.setLatitude(latitude);

        assertEquals(latitude, location.getLatitude());
    }

    @Test
    public void testSetLongitude() {
        double longitude = -123;
        location.setLongitude(longitude);

        assertEquals(longitude, location.getLongitude());
    }



}
