package com.kuwe.project.Observer.core;

import java.util.ArrayList;
import java.util.List;

public abstract class NutritionValue {
    protected Game kue;
    protected String nutritionType;
    private List<Menu> menu = new ArrayList<>();

    public abstract void updateMenu();

    public List<Menu> getMenus() {
        return this.menu;
    }


}
