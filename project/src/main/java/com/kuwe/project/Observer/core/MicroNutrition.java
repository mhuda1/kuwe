package com.kuwe.project.Observer.core;

public class MicroNutrition extends NutritionValue{
    public MicroNutrition (Game kue){
        this.nutritionType = "MicroNutrition";
        this.kue = kue;
    }

    @Override
    public void updateMenu(){
        this.getMenus().add(this.kue.getMenu());

    }
}
