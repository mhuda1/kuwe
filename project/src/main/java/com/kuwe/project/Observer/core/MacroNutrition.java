package com.kuwe.project.Observer.core;

public class MacroNutrition extends NutritionValue {

    public MacroNutrition (Game kue){
        this.nutritionType = "MacroNutrition";
        this.kue = kue;
    }

    @Override
    public void updateMenu(){
        this.getMenus().add(this.kue.getMenu());

    }


}
