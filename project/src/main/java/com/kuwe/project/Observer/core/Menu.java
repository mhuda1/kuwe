package com.kuwe.project.Observer.core;

public class Menu {

    private String namaBahan;
    private String tipeNutrisi;

    public void setNamaBahan(String namaBahan) {
        this.namaBahan = namaBahan;
    }

    public void setTipeNutrisi(String tipeNutrisi) {
        this.tipeNutrisi = tipeNutrisi;
    }

    public String getNamaBahan() { return this.namaBahan; }

    public String getTipeNutrisi() {
        return this.tipeNutrisi;
    }

}
