package com.kuwe.project.Observer.core;

import java.util.ArrayList;
import java.util.List;

public abstract class Info {
    // Spoonacular documentation : https://spoonacular.com/food-api/docs#Search-Recipes
    protected String api;
    protected Game statusgame;
    public List<String> ingredients = new ArrayList<>();

    public abstract void update();

    public String getApi() {
        return this.api;
    }

    public List<String> getIngredients() {
        return this.ingredients;
    }



}
