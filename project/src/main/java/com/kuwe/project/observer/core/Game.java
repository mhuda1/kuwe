package com.kuwe.project.Observer.core;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private List<Info> infos = new ArrayList<>();
    private List<String> ingredients = new ArrayList<>();
    private String gameStatus;

    List<NutritionValue> listNutrition = new ArrayList<>();
    private Menu menu;
//    List<String> listMenu = new ArrayList<>();

    public Game() { this.gameStatus = "not Done"; }
    public void add(Info info) { infos.add(info); }

    public void addIngredient(String ingredient) {
        getIngredients().add(ingredient);
        broadcast();
    }

    public String getGameStatus () {return this.gameStatus;}

    public void setGameStatus() {this.gameStatus = "Done";}

    public List<String> getIngredients() {return this.ingredients;}

    private void broadcast() {
        for (Info api : infos) {
            api.update();
        }
    }

    public void add(NutritionValue nutrition) {
        listNutrition.add(nutrition);
    }

    public void addMenu(Menu menu) {
        this.menu = menu;
        notifyUser();
    }

//    public String getTipeNutrisi () {return menu.getTipeNutrisi();}

    public Menu getMenu() {return this.menu;}

    public List<NutritionValue> getNutritionValueList() {
        return listNutrition;
    }


    public void notifyUser(){
        for (NutritionValue nutrition: this.listNutrition) {
            nutrition.updateMenu();
        }
    }





    //    public void addNutrition(NutritionValue n){
//        this.listNutrition.add(n);
//    }
//
//
//    public void removeNutrition(NutritionValue n){
//        this.listNutrition.remove(n);
//    }







}