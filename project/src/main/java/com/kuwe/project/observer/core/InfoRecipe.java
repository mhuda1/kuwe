package com.kuwe.project.Observer.core;

import java.util.ArrayList;
import java.util.List;

public class InfoRecipe extends Info {

    public InfoRecipe(Game kue) {
        this.api = "spoonacular";
        this.statusgame = kue;
        kue.add(this);
    }

    //ToDo: Complete Me
    @Override
    public void update() {
        if (this.statusgame.getGameStatus().equals("Done")) {
            this.ingredients = statusgame.getIngredients();
        }
    }
}

