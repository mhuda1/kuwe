package com.kuwe.project.storelocation.controller;

import com.kuwe.project.storelocation.core.Location;
import com.kuwe.project.storelocation.service.storeLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Controller
public class StoreLocationController {

//    @Autowired
//    private storeLocationService storeLocationservice;

    @GetMapping("/find-form")
    public String findstore(Model model){
        model.addAttribute("location", new Location());
        return "findStoreForm";
    }

    @PostMapping("/find-form")
    public String greetingSubmit(@ModelAttribute Location location) {
        return "storeList";
    }
}
