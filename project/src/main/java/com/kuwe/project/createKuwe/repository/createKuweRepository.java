package com.kuwe.project.createKuwe.repository;

import com.kuwe.project.createKuwe.core.kuwe.Kuwe;
import com.kuwe.project.createKuwe.core.toppings.Topping;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class createKuweRepository {

    public void addToppingToKuwe(ArrayList<Kuwe> kuwes, String topping, String kuweName){

        for(Kuwe kuwe : kuwes){
            if (kuwe.getNamaKuwe().equals(kuweName)){

                if (kuweName.equals("Kuwe Coklat")){
                    Kuwe coklat = new Kuwe();

                    if (topping.equals("chocolate")){
                       coklat = Topping.CHOC_TOPPING.addTopping(coklat);
                    }
                    else if (topping.equals("strawberry")){
                        coklat = Topping.STRW_TOPPING.addTopping(coklat);
                    }
                }

                if (kuweName.equals("Kuwe Vanilla")){
                    Kuwe vanilla = new Kuwe();

                    if (topping.equals("chocolate")){
                        vanilla = Topping.CHOC_TOPPING.addTopping(vanilla);
                    }
                    else if (topping.equals("strawberry")){
                        vanilla = Topping.STRW_TOPPING.addTopping(vanilla);
                    }
                }

                int index = kuwes.indexOf(kuwe);
                kuwes.set(index, kuwe);
            }
        }

    }
}
