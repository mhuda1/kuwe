package com.kuwe.project.createKuwe.core.toppings;

import com.kuwe.project.createKuwe.core.kuwe.Kuwe;

import java.util.ArrayList;

public class chocolate extends Kuwe{

    Kuwe kuwe;

    public chocolate(Kuwe kuwe){
        this.kuwe = kuwe;
    }

    @Override
    public String getNamaKuwe() { return kuwe.getNamaKuwe(); }

    @Override
    public String getKuweDescription() { return kuwe.getKuweDescription(); }

    @Override
    public ArrayList<String> getToppings() {
        String topping = "Chocolate";
//        if (kuwe == null) return ;
        kuwe.getToppings().add(topping);

        return kuwe.getToppings();
    }
}
