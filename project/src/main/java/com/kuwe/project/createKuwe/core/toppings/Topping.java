package com.kuwe.project.createKuwe.core.toppings;

import com.kuwe.project.createKuwe.core.kuwe.Kuwe;

public enum Topping {
    CHOC_TOPPING,
    STRW_TOPPING;


    public Kuwe addTopping(Kuwe kuwe){

        if (this == Topping.CHOC_TOPPING){
            kuwe = new chocolate(kuwe);
        } else if (this == Topping.STRW_TOPPING){
            kuwe = new strawberry(kuwe);
        } else {
            kuwe = kuwe;
        }

        return kuwe;
    }
}
