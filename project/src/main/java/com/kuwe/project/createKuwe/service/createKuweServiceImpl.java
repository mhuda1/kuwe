package com.kuwe.project.createKuwe.service;

import com.kuwe.project.createKuwe.core.kuwe.Coklat;
import com.kuwe.project.createKuwe.core.kuwe.Kuwe;
import com.kuwe.project.createKuwe.core.kuwe.Vanilla;
import com.kuwe.project.createKuwe.repository.createKuweRepository;

import java.util.ArrayList;

public class createKuweServiceImpl implements createKuweService{

    private ArrayList<Kuwe> Kuwe;
    private createKuweRepository createKuweRepository;

    public createKuweServiceImpl(ArrayList<Kuwe> kuwe){
        this.Kuwe = kuwe;
        createKuwe();
    }

    public void createKuwe(){
        Kuwe coklat = new Coklat();
        Kuwe vanilla = new Vanilla();
        Kuwe.add(coklat);
        Kuwe.add(vanilla);
    }

    @Override
    public void addToppings(String topping, String kuweName){
        createKuweRepository.addToppingToKuwe(Kuwe, kuweName, topping);
    }

    @Override
    public Iterable<Kuwe> getAllKuwe() {
        return Kuwe;
    }

}
