package com.kuwe.project.createKuwe.core.kuwe;

import java.util.ArrayList;

public class Vanilla extends Kuwe{

    public Vanilla(){
        this.namaKuwe = "Kuwe Vanilla";
        this.kuweDescription = "Kuwe vanilla maknyus";
        this.toppings = new ArrayList<>();
    }
}
