package com.kuwe.project.createKuwe.service;

import com.kuwe.project.createKuwe.core.kuwe.Kuwe;

public interface createKuweService {

    public void addToppings(String topping, String kuwe);
    public Iterable<Kuwe> getAllKuwe();
}
