package com.kuwe.project.createKuwe.core.kuwe;

import com.kuwe.project.createKuwe.core.toppings.Topping;

import java.util.ArrayList;

public class Kuwe {

    protected String namaKuwe;
    protected String kuweDescription;
    protected ArrayList<String> toppings;

    public String getNamaKuwe() { return namaKuwe; }

    public String getKuweDescription() { return kuweDescription; }

    public ArrayList<String> getToppings() { return toppings; }
}
